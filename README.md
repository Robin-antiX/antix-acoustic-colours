# antiX acoustic colours

antiX acoustic colours is a tiny script allowing to change the acoustic colour of audio output. Think of it what you know from tube radios, they had buttons to switch easily between different acoustic colours like "bass", "treble", "solo", "orchestra", "jazz" etc. This concept is what antix acoustic colours is based on, providing these possibilities of fast switching the sound profile on modern PCs.

## Installation

A.) Apt package installation (This is the preferred way of installation.)
- [ ] Download deb-installer-package and checksum file from respective folder. It was tested on antiX 23, 32bit and 64bit both.
- [ ] Check shasum file using the command:
```
shasum -c './antix-acoustic-colours.deb.sha512.sum'
```
It must show an _OK_.
- [ ] If check was OK, install the package by the commands 
```
sudo apt-get update && sudo apt-get install './antix-acoustic-colours.deb'
```
Check carefully console output for error messages during installation.

On first start after install antiX acoustic colours will copy over all needed filter and preset files from /etc/skel to your home folder. Existing files of same name found to be present already in personal easyefects config folder will be moved to .bak extension beforehand. Thenceforth antiX acoustic colours will merely check for missing files on startup and resore them, while existing user modified presets won't be touched.

B.) Manual installation (not recommended):
- [ ] Put the script file _antix-acoustic-colours.sh_ from _bin_ directory into the antiX system folder _/usr/local/bin_ and make it executable (_sudo chmod 755 /usr/local/bin/antix-acoustic-colours.sh_)
- [ ] Put the localisation of its user interface for your language (_.mo_ type file) into the respective locale subflolder of your antiX system. ( _/usr/share/locale/‹your_language_ID›/LC_MESSAGES_ ).
- [ ] Put all the files found in /resources/config to the /etc/skel/.config/easyeffects system folder.
- [ ] Put the file antiXac.svg to the system folder /usr/shares/icons/highcolor/scalable/apps
- [ ] Make sure you have _easyeffects (>= 7.1.3-1)_, _lsp-plugins-lv2 (>=1.2.14-1)_, _zam-plugins (>=4.2-1)_, _mda-lv2 (>=1.2.10-2)_, _calf-plugins_ and _libglib2.0-bin_ installed.

## Usage
Either click on the menu entry in antiX program menu, or enter _antiX-acoustic-colours_ in a command line window, e.g. in RoxTerm.
In the GUI coming up click a button to switch to another acoustic colour.
You can manually adjust and fine tune all the colour presets by selecting one of the acoustic colour buttons and starting easysffects from the antiX main menu then. Do all what you like in easyeffects then, you can even add or remove filters from the processing chain, readjust slider settings etc. After being done with modifying the current preset according to your predilections save the changed settings for the very preset, from within easysffects to the very preset name. This will overwrite the former one in your home folder. (You can always restore a virgin version of the file from /etc/skel if needed.) If you just want to play around and explore the vast possibilities of sound shaping, just do this in easyeffects, starting from one of the presets. As long you don't save the changes, they will be simply reset to the stored values when loading another preset, e.g. when switching to another acoustic colour in antiX acoustic colours.

You may close antiX acoustic colours window, the selected acoustic colour will stay active until you switch to another one either by again running the antiX Acoustic Colors script or by selecting it from easyeffects pulldown menus directly. As well you can keep antiX Acoustic Colours window open on screen or minimised to tray to allow fast access to colour switching. The script itself consumes very low resources merely.

antiX acoustic colours will make sure easyeffects is running in proper mode on each startup, so it might be restarted possibly if found to be running in wrong mode.

Additional hint: For recording purposes by other tools like e.g. audacity or even arecord it's a good idea to set your recording software to use pipewire's monitor lines rather than the processed default audio lines. This is to be done equivalently as you might be familiar with from your analog audio devices like tape decks, CD players or other HiFi equipment. Only if you want to have the effects present within your recording, select the default audio line as recording source. Please be aware the effects will be possibly applied a second time on playback later when played, which might be fine for achieving special effects in your recordings, even when your recording software doesn't provide these features on it's own, but for default recording tasks it should be disapproved generally. It might lead to poor recording quality if not done very carefully. (The before said doesn't go for the antiX scripts antiXradio and aCSTV, which are designed to take always from the monitor streams directly without any audio processing beforehand.)

## Known Issues
- While the current versions of pipewire and easyeffects set as dependency in antiX acoustic colours 0.4 have been proven to work stable on 32 bit hardware finally, pipewire's additional audio transport layer still can easyly outpower weak systems, triggering distorted sound (short audio interruptions of some ms silence, crackling and spattering noises when CPU or other system resouces come near their limits; This behaviour od pipewire has been found on powerfull 64 bit multicore devices as well, when streesing the hardware with some other tasks like heavy data transfer to disks or heavy CPU load, while audio is playing with pipewire; this doesn't occur when running plain alsa).
- Easyeffects filters have been replaced from it's former to current version, so most presets of antiX acoustic colours 0.3 fail to work properly in 0.4. You have to use the presets which come with the respective acoustic colour version always. **Hence please make sure either to delete all the files in your ~/.config/easyeffects folder, or remove the ~/.config/antiX-acoustic-colours folder (containing a single hidden flagfile) before starting the updated version for the first time.** Otherwise the colours won't be fully applied or fail to work completely.

## Plans
Probably one of the future versions will come with additional plain alsa support. First tests done for this are promising.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Contributing
- If you find a translation of user interface or user manual being inadequat or misleading wrong, please edit it simply on [antix-development at transifex](https://www.transifex.com/anticapitalista/antix-development/antix-acoustic-colours/). On transifex you may edit the string translations used in user interface directly.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2024 by Robin.antiX for antiX community.
