��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (     �     �     �  
   �     	               (  F   G     �     �     �     �     �                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2024
Language-Team: Spanish (https://app.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Bajos Catedral Jazz Silenciado Sin Pipewire. Aceptar Orquesta Pipewire no está funcionando. Por favor, vaya al centro de control antiX\n\te inicie Pipewire antes. Solista Discurso Agudos ¡Advertencia! Tonos acústicos antiX 