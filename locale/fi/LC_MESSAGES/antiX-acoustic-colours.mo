��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (     �  	   �     �     �     �     �  	   �       <        \     a     h  	   o     y                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Kimmo Kujansuu <mrkujansuu@gmail.com>, 2024
Language-Team: Finnish (https://app.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Bass Cathedral Jazz Muffled Ei Pipewireä OK Orchestra Pipewire ei ole käynnissä. Mene antiX ohjauskeskukseen ja\n\käynnistä ensin Pipewire. Solo Speech Treble Varoitus! antiX värimaailma 