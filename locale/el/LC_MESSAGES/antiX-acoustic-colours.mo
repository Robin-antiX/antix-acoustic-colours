��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (     �  %   �               (     =     L  <   ]  d   �     �               )  '   9                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2024
Language-Team: Greek (https://app.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Μπάσσο Εκκλησιαστικός ήχος Τζαζ πνιγμένος ήχος Χωρίς Pipewire. Εντάξει Ορχήστρα Το Pipewire δεν είναι ενεργοποιημένο Μεταβείτε στο κέντρο ελέγχου antiX και ξεκινήστε το Pipewire. Σόλο Ομιλία Πρίμα ήχος Προσοχή! ακουστικά χρώματα antiX 