��          �      ,      �     �  	   �     �     �     �     �     �  	   �     �  ?   �               %     ,     4     =  �  T     �  	   �     �     �                 	        $  \   <     �     �     �  
   �     �     �                                    
                                      	    Bass Cathedral Jazz Muffled No Pipewire. OK Off Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Unknown Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2024
Language-Team: Albanian (https://app.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Bass Katedrale Xhazz E mbytur Pa Pipewire. OK Off Orkestër Pipewire nuk po xhiron. Ju lutemi, më përpara kaloni te qendra e kontrollit të antiX-it\n\tdhe nisni Pipewire-in. Solo E folur Treble E panjohur Kujdes! Ngjyra akustike antiX 