��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (     �     �                    )  	   1     ;  W   W     �     �     �  
   �  #   �                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Wallon Wallon, 2024
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Basse Cathédrale Jazz En sourdine Pas de Pipewire. Valider Orchestre Pipewire ne fonctionne pas. Veuillez vous rendre dans le centre de\n\tcontrôle d’antiX et lancer Pipewire avant. Solo Discours Aigu Attention! Égalisateur antiX acoustic colours 