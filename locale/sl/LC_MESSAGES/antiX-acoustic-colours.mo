��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (                 	             .     5     >  <   R     �     �     �  
   �     �                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2024
Language-Team: Slovenian (https://app.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Bas Cerkvena Jazz Zadušeno Manjka Pipewire. V redu Orkester Pipewire ni zagnan. Pred tem zaženite Pipewire\niz antiX nadzornega središča. Solo Govor Visoko Opozorilo! antiX akustične barve 