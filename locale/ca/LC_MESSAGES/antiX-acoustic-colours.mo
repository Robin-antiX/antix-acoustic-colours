��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                 �  (     �     �     �     �     �     �     �       K        d     i     m  	   s     }                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2024
Language-Team: Catalan (https://app.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Greus Catedral Jazz Sordina Sense Pipewire. D'acord Orquesta Pipewire no s'executa. Si us plau, aneu al Centre de Control\n\td'antiX i engegueu Pipewire abans. Solo Veu Aguts Atenció! antiX acoustic colours 