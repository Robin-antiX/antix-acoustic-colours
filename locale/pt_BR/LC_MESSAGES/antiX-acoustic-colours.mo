��          �      ,      �     �  	   �     �     �     �     �     �  	   �     �  ?   �               %     ,     4     =  �  T     +     1     :     ?  2   G     z     }  	   �  7   �  [   �     #     (     ,     2     ?  +   F                                    
                                      	    Bass Cathedral Jazz Muffled No Pipewire. OK Off Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Unknown Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2024
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Grave Catedral Jazz Abafado O servidor de áudio PipeWire não foi encontrado. OK Ocultar Orquestra O servidor de áudio PipeWire não está em execução. Por favor, primeiro inicie o servidor de áudio\n\tPipeWire no centro de controle do antiX. Solo Voz Agudo Desconhecido Aviso! Controle de Timbres da Ambientação do Som 