��          �      ,      �     �  	   �     �     �     �     �     �  	   �     �  ?   �               %     ,     4     =  �  T     �     �     �     �     �                      H   2     {     �     �     �     �     �                                    
                                      	    Bass Cathedral Jazz Muffled No Pipewire. OK Off Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Unknown Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2024
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Bas Katedral Jazz Dämpad Ingen Pipewire. OK Av Orkester Pipewire körs inte. Var vänlig gå till antiX kontrollcenter\n\toch starta Pipewire först. Solo Tal Diskant Okänd Varning! antiX akustiska färger 