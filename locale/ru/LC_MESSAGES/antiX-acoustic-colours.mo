��          �            h     i  	   n     x     }     �     �  	   �     �  ?   �     �     �                   (     :  
   A     L     U     n          �     �  �   �     :     C     L     j  %   �                                     	                                    
       Bass Cathedral Jazz Muffled No Pipewire. OK Orchestra Pipewire not running. Please go to antiX control centre\n\tand start Pipewire before. Solo Speech Treble Warning! antiX acoustic colours Project-Id-Version: antiX acoustic colours 0.3
Report-Msgid-Bugs-To: www.antixforum.com
PO-Revision-Date: 2024-01-10 19:01+0000
Last-Translator: Victor Red, 2024
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Бас Собор Джаз Приглушенный Нет Pipewire. ОК Оркестр Pipewire не запущен. Пожалуйста, перейдите в центр управления antiX\n\tи запустите Pipewire перед этим. Соло Речь Высокие частоты Предупреждение! antiX звуковая палитра 